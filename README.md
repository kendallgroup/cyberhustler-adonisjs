## CyberHustler Project - Underground Media Company / Music Platform

Introduction to Cyberhustler Project

Using a HTML template we are creating a Platform for Atlanta Aritst to have the ability to promote there work, advertize and find the people necessary in order to move their artwork forward as it pertains to Social Media, media content and Music creation and production.
We are asking artist to create an account upload there music, images and link to allow us to promote your music to the people in the music scence here in Atlanta Georgia.
***Updated****
The Radio Station aspect of the Platform has been removed temporary or maybe forever.

We decided to use a NodeJs platform for our Cyberhustler project.
Information about the plartform is below.

# Development Stack

1. Language - NodeJS
2. Database - MySQL
3. Template Engine - EDGE Templates
4. LUCID ORM - Querying Database
5. Platform - AdonisJs Version 4.1 - Will upgrade to 5.0 when released.
6. Adonis Comes with all the below Application Additions


# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds


