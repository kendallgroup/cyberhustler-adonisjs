'use strict'

class User {
  get rules () {
    return {
      // validation rules
      username:'required',
      email: 'required|email|unique:users',
      password: 'required|min:5|confirmed'
    }
  }

  get messages () {
    return {
      'username.required': 'Username is a required field',
      'email.required': 'You must provide a email address.',
      'email.email': 'You must provide a valid email address.',
      'email.unique': 'This email is already registered.',
      'password.required': 'The password field is required',
      'password.min': 'The password field must be at least 5 characters',
      'password.confirmed': 'The password fields do not match'
    }
  }
}

module.exports = User
