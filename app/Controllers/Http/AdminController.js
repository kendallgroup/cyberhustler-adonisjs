'use strict'

const Slideshow = use('App/Models/Slider')
const Database = use('Database')

class AdminController {
    async index({ view }){
        return view.render('partials.adminhome');
    }

    async slideshow({ view, request, response }){
        //Get all Records from table
        const slides = await Slideshow.all()

        return view.render('adminpages.slideshow', { slides: slides });
    }

    async storeslideshow({ view, request, response }){
        //Grab request data
        const data = request.post()
        //record to console
        console.log(data)

        //convert checkbox value
        if(data.ShowOnHomepage === 'on'){
            data.ShowOnHomepage = 1;
        } else {
            data.ShowOnHomepage = 2;
        }
        //Insert Data in the table
        const sliderdata = await Slideshow.create(data)

        //Pull all data from database table Slideshow to show in table.
        //change this to sort by date to show the last entries.
        const slides = await Database.table('sliders').orderBy('created_at', 'desc');
        console.log(slides);

        return view.render('adminpages.slideshow', { slides: slides });
    }

    async slideshowEdit({ view , params }){
        const slide = Database.table('sliders').where('ID', params.id)
        return view.render('adminpages.editSlideshow', { slides: slide })
    }

    async upcomingshows({ view }){
        return view.render('adminpages.upcommingshows');
    }

    async storeupcomingshows({ view }){
        return view.render('adminpages.upcommingshows');
    }

    async videoitems({ view }){
        return view.render('adminpages.videoitems');
    }

    async storevideoitems({ view }){
        return view.render('adminpages.storevideoitems');
    }

    async hustlerschart({ view }){
        return view.render('adminpages.hustlerschart');
    }

    async storehustlerschart({ view }){
        return view.render('adminpages.storehustlerschart');
    }

    async storehustlerschart({ view }){
        return view.render('adminpages.storehustlerschart');
    }

    async herovideo({ view }){
        return view.render('adminpages.herovideo');
    }

    async storeherovideo({ view }){
        return view.render('adminpages.storeherovideo');
    }

    async blog({ view }){
        return view.render('adminpages.blog');
    }
}

module.exports = AdminController
