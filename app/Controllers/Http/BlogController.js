'use strict'

const Blog = use('App/Models/Blog')
const Database = use('Database')

class BlogController {
    async index({ view, request, response }){
        const page = {}
        const blogs = await Database.table('blogs').where('ShowOnHomePage', 1)
        const post = await Blog.findBy('id', 4)
        page.blogs = blogs
        page.post = post
        return view.render('FrontEnd.Bloglist', { page : page })
    }

    async blog({ view, params, request, response }){
        const page = {}
        const id = params.id
        console.log(id)
        const blog = await Blog.findBy('id', id)
        const post = await Blog.findBy('id', 8)
        console.log(blog)
        page.blog = blog
        page.post = post
        return view.render('FrontEnd.blogTemplate', { page: page })
    }
}

module.exports = BlogController
