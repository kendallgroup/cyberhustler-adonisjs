'use strict'

const Video = use('App/Models/Video')
const Database = use('Database')
const moment = use('moment')

class AdminVideoController {
    async index({ view }){
        const video = await Video.all()
        //console.log(video)
        return view.render('adminpages.AdminVideo.videoIndex', { video: video })
    }

    async create({ view }){
        return view.render('adminpages.AdminVideo.videoCreate')
    }

    async store({ view, request }){
        const data = request.except('_csrf')
        console.log(data)

         //convert checkbox value
         if(data.ShowOnHomePage === 'on'){
            data.ShowOnHomePage = 1;
        } else {
            data.ShowOnHomePage = 2;
        }
        const video = await Video.create(data)
        return view.render('adminpages.AdminVideo.videoShow', { video: video })
    }

    async update({ view, request, params }){
        const id = params.id
        const data = request.all()
        const show = data.ShowOnHomePage == null ? 2 : 1

        const video = await Database
            .table('videos')
            .where('id', id)
            .update({
                Category : data.Category,
                Title : data.Title,
                VideoUrl : data.VideoUrl,
                Link : data.Link,
                ImgUrl : data.ImgUrl,
                ShowOnHomePage : show,
                updated_at : moment().format()
            })

        console.log('testing')
        const result = await Video.findBy('id', id)
        return view.render('adminpages.AdminVideo.videoShow', { video: result })
    }

    async show({view, request, params}){
        console.log('Show Video display')
        const id = params.id
        const post = request.all()
        console.log(post)
        const video = await Video.findBy('id', id)
        return view.render('adminpages.AdminVideo.videoShow', { video: video})
    }

    async edit({view, params}){
        const id = params.id
        const video = await Video.find(id)
        return view.render('adminpages.AdminVideo.videoEdit', { video: video})
    }

    async destroy({view, params}){
         //Get it of row to delete
         const id = params.id
         //Delete Sliders with id
         await Database.table('videos').where('id', id).delete()
         return view.render('adminpages.AdminVideo.videoIndex')
    }
}

module.exports = AdminVideoController
