'use strict'

const Chart = use('App/Models/Chart')
const Header = use('App/Models/Chartheader')
const Database = use('Database')
const Blog = use('App/Models/Blog')

class HustlersChartController {
    async index({ view, request }){
        const chart = await Database.table('charts').where('ShowOnHomepage', 1).orderBy('Rank', 'asc')
        const post = await Blog.findBy('id', 2) //Database.table('blogs').where('id', 2)
        const header = await Header.findBy('id', 3)
        const page = {}
        page.chart = chart
        page.post = post
        page.header = header
        console.log(post)
        return view.render('FrontEnd.HustlersChartTemplate', { page : page })
    }

    async HustlersChart({ view }){
        return view.render('')
    }
}

module.exports = HustlersChartController
