'use strict'

const Shows = use('App/Models/Upcomingshow')
const Database = use('Database')
const moment = use('moment')

class AdminUpcomingShowController {
    async index({ view, response, request, params} ){
        const shows = await Shows.all()
        console.log(shows)
        return view.render('adminpages.UpcomingShows.showIndex', { shows: shows});
    }

    async create( {view, response, request} ){
        return view.render('adminpages.UpcomingShows.showCreate')
    }

    async store( {view, response, request} ){
          //Grab request data
          const data = request.except('_csrf')
          //record to console
          //console.log(data)
  
         //convert checkbox value
         if(data.ShowOnHomePage === 'on'){
            data.ShowOnHomePage = 1;
        } else {
            data.ShowOnHomePage = 2;
        }
          console.log(data)
          //Insert Data in the table
          const shows = await Shows.create(data)
          console.log(shows.$attributes.id)
          const show = await Shows.findBy('id', shows.$attributes.id)
          //const slide = slides.$attributes 
         return view.render('adminpages.UpcomingShows.showtheshow', { show: show })
    }

    async show( {view, response, request, params}){
        const id = params.id
        const show = await Shows.findBy('id', id)
        return view.render('adminpages.UpcomingShows.showtheshow', { show: show })
    }

    async edit( {view, response, request, params}){
        const id = params.id
        const show = await Shows.findBy('id', id)
        return view.render('adminpages.UpcomingShows.showEdit', { show: show })
    }

    async update( {view, response, request, params}){
        const id = params.id
        const data = request.all()
        const showOn = data.ShowOnHomepage == null ? 2 : 1

        const show = await Database
            .table('upcomingshows')
            .where('id', id)
            .update({
                Category : data.Category,
                Title : data.Title,
                ShowHost : data.ShowHost,
                Link : data.Link,
                ShowTime: data.ShowTime,
                ImgUrl : data.ImgUrl,
                ShowOnHomepage : showOn,
                updated_at : moment().format()
            })

        console.log('Upcoming shows')
        const result = await Shows.findBy('id', id)
        return view.render('adminpages.UpcomingShows.showtheshow', { show: result})
    }

    async destroy( {view, response, request, params}){
        //Get it of row to delete
        const id = params.id
        //Delete Sliders with id
        await Database.table('upcomingshows').where('id', id).delete()
        return view.render('adminpages.UpcomingShows.showIndex')
    }

    async homePage(data){
        return data === 'on' ?  data = 1:  data = 2;
    }
}

module.exports = AdminUpcomingShowController
