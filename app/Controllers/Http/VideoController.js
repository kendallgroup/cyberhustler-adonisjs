'use strict'

const Video = use('App/Models/Video')
const Database = use('Database')

class VideoController {
    async index({ view }){
        const videos = await Database.table('videos')
        console.log(videos)
        return view.render('FrontEnd.VideoArchieve', { page : videos })
    }

    async video({ view }){
        return view.render('FrontEnd.videoTemplate')
    }
}

module.exports = VideoController
