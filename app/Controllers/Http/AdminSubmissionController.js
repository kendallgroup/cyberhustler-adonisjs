'use strict'

const Submissions = use('App/Models/TrackSubmission')

class AdminSubmissionController {
    async index({view}){
        const submissions = await Submissions.all()
        return view.render('adminpages.AdminSubmission.submissionIndex', { page : submissions })
    }

    async edit({view, request, params, response}){
        //development Only
        const id = params.id
        const submission = await Submissions.findBy('id', id)
        return view.render('adminpages.AdminSubmission.submissionEdit', { page : submission })
    }

    async update({view, request, params, response}){
        const id = params.id
        const data = request.all()
        const submission = await Submissions.findBy('id', id)

        submission.FirstName = data.FirstName
        submission.LastName = data.LastName
        submission.ArtistName = data.ArtistName
        submission.Email = data.Email
        submission.TrackName = data.TrackName
        submission.AudioUrl = data.AudioUrl
        submission.VideoUrl = data.VideoUrl
        submission.ArtistLink = data.ArtistLink

        await submission.save()
        return view.render('adminpages.AdminSubmission.submissionShow', { page : submission })
    }

    async destroy({view, request, params, response}){
        const id = params.id
        const submission = await Submissions.findBy('id', id)
        await submission.delete()
        return view.render('adminpages.AdminSubmission.submissionIndex', { page: submission })
    }
}

module.exports = AdminSubmissionController
