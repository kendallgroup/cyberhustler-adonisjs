'use strict'

const Event = use('App/Models/Event')
const Blog = use('App/Models/Blog')
const Database = use('Database')

class EventController {
    async index({ view }){
        const event = await Database.table('events')
        console.log(event)
        return view.render('FrontEnd.EventsArchieve', { page : event })
    }

    async event({ view, params, request }){
        const id = params.id
        const page = {}
        const event = await Event.findBy('id', id)
        const post = await Blog.findBy('id', 3)
        page.post = post
        page.event = event
        console.log(post)
        return view.render('FrontEnd.eventTemplate', { page : page })
    }
}

module.exports = EventController
