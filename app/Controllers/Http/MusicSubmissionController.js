'use strict'

const Submit = use('App/Models/TrackSubmission')

class MusicSubmissionController {
    async index({ view }){
        return view.render('FrontEnd.MusicSubmission');
    }

    async store({ view, request, response }){
        const param = request.except('_csrf')
        console.log(param)
        const submit = await Submit.create(param)
        return view.render('FrontEnd.MusicSubmission')
    }

    async show({}){
        return view.render('')
    }
}

module.exports = MusicSubmissionController
