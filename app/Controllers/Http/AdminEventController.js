'use strict'

const Event = use('App/Models/Event')
const Database = use("Database")
const moment = use('moment')

class AdminEventController {
    async index({ view, request, response, params }){
        const event = await Event.all()
        return view.render('adminpages.AdminEvents.EventIndex', { event: event})
    }

    async create({ view, request }){
        return view.render('adminpages.AdminEvents.EventCreate')
    }

    async store({ view, request, params }){

        const data = request.except('_csrf')

         //convert checkbox value
         if(data.ShowOnHomePage === 'on'){
            data.ShowOnHomePage = 1;
        } else {
            data.ShowOnHomePage = 2;
        }

        const event = await Event.create(data)
        return view.render('adminpages.AdminEvents.EventShow', { event : event })
    }

    async show({ view, request, params }){
        const id = params.id
        const event = await Event.find(id)
        return view.render('adminpages.AdminEvents.EventShow', { event: event })
    }

    async edit({ view, request, params }){
        const id = params.id
        const event = await Event.find(id)
        moment(event.EventDate).format('MM/DD/YYYY')
        return view.render('adminpages.AdminEvents.EventEdit', { event : event })
    }

    async update({ view, request, params }){
        const id = params.id
        console.log('Event Update')
        const changes = request.all()
        const show = changes.ShowOnHomePage == null ? 2 : 1
        
        const event = await Database
                .table('events')
                .where('id', id)
                .update({
                    EventName : changes.EventName,
                    Host : changes.Host,
                    Description : changes.Description,
                    Address : changes.Address,
                    City : changes.City,
                    State : changes.State,
                    EventTime : changes.EventTime,
                    EventDate : changes.EventDate,
                    Link : changes.Link,
                    ImgUrl : changes.ImgUrl,
                    ShowOnHomepage : show,
                    updated_at : moment().format()
                })
        
        console.log(moment().format())
                
        const result = await Event.findBy('id', id)
        return view.render('adminpages.AdminEvents.EventShow', { event : result })
    }

    async destroy({ view, request, params }){
        return view.render('adminpages.AdminEvents.EventIndex')
    }
}

module.exports = AdminEventController
