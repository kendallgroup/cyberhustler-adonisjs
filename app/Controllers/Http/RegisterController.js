'use strict'

const User = use('App/Models/User')
const { validate } = use('Validator')

class RegisterController {

    async create({ view, auth, response }){

        try {
            await auth.check()
          } catch (error) {
              //User is not logged in
              return view.render('auth.register')
          }
          //User is logged in
          return response.redirect('/')
    }

    async store({ response , request, session }){
        console.log(validate)
        try
        {
            const user = await User.create({
                username: request.input('username'),
                email: request.input('email'),
                password: request.input('password')
            })
    
        }catch(error){
            console.log(error)  
            session.flash({ errorMessage: error})
        }
       
        console.log(request.all())
        console.log('breakpoint')

        session.flash({ successMessage: 'You have registered succesfully!'})
        return response.redirect('/register')
    }
}

module.exports = RegisterController
