'use strict'

const Chart = use('App/Models/Chart')
const moment = use('moment')
const crypto = use('crypto-hash')

class AdminChartController {
    async index({ view, response, request} ){
        const chart = await Chart.all()
        return view.render('adminpages.AdminCharts.chartIndex', { chart: chart});
    }

    async create( { view } ){
        return view.render('adminpages.AdminCharts.chartCreate')
    }

    async store( {view, request} ){
        const data = request.except('_csrf')
        //Create hashID from the name of the chart


         //convert checkbox value
         if(data.ShowOnHomePage === 'on'){
            data.ShowOnHomePage = 1;
        } else {
            data.ShowOnHomePage = 2;
        }

        const chart = await Chart.create(data)
        return view.render('adminpages.AdminCharts.chartShow', { chart: chart})
    }

    async show( {view, response, request, params}){
        const id = params.id
        const chart = await Chart.find(id)
        return view.render('adminpages.AdminCharts.chartShow', { chart: chart})
    }

    async edit( {view, response, request, params}){
        const id = params.id
        const chart = await Chart.find(id)
        return view.render('adminpages.AdminCharts.chartEdit', { chart: chart})
    }

    async update( {view, response, request, params}){
       const id = params.id
       const data = request.all()
       const chart = await Chart.findBy('id', id)
       const show = data.ShowOnHomePage == null ? 2 : 1

       chart.ArtistName = data.ArtistName
       chart.TrackName = data.TrackName
       chart.VideoUrl = data.VideoUrl
       chart.Rank = data.Rank
       chart.Icon = data.Icon
       chart.ImgUrl = data.ImgUrl
       chart.PurchaseLink = data.PurchaseLink
       chart.ChartName = data.ChartName
       chart.HashID = data.HashID
       chart.ShowOnHomePage = show

       await chart.save()
       return view.render('adminpages.AdminCharts.chartShow', { chart: chart})
    }

    async destroy({view, response, request, params}){
        const id = params.id
        const chart = await Chart.findBy('id', id)
        await chart.delete()
        return view.render('adminpages.AdminVideo.videoIndex')
    }


}

module.exports = AdminChartController
