'use strict'

const section = use('App/Models/section')
const { sha256 } = require('crypto-hash')
const Slider = use('App/Models/Slider')
const Database = use('Database')

class SectionController {
    async home({ view }){
        const homeObject = {}
        //Get Sliders
        const slider = await Database.table('Sliders').where('ShowOnHomepage', 1)
        homeObject.slider = slider;
        const shows = await Database.table('upcomingshows').where('ShowOnHomepage', 1)
        homeObject.shows = shows
        const chart = await Database.table('charts').where('ShowOnHomePage', 1).orderBy('Rank', 'asc')
        homeObject.chart = chart
        const blog = await Database.table('blogs').where('ShowOnHomePage', 1)
        homeObject.blog = blog
        const video = await Database.table('videos').where('Category', '=', 'Atlanta Muzic')
        homeObject.video = video
        return view.render('FrontEnd.Home-index', { homepage: homeObject});
    }

    async errorpage({ view , request }){
        return view.render('error')
    }
}

module.exports = SectionController
