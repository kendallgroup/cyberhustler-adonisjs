'use strict'

class HomePageController {
    async player({ view }){
        return view.render('PopupPlayer');
    }
}

module.exports = HomePageController
