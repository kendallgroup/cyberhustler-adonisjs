'use strict'

const Slideshow = use('App/Models/Slider')
const Database = use('Database')
const Route = use('Route')
const moment = use('moment')

class AdminSlideShowController {
    async index({ view, response, request} ){
        const slides = await Slideshow.all()
        return view.render('adminpages.Slideshows.slideshowIndex', { slides: slides })
    }

    async create( {view, response, request} ){
        return view.render('adminpages.Slideshows.slideshowStore')
    }

    async store( {view, response, request} ){
         //Grab request data
         const data = request.except('_csrf')
         //record to console
         //console.log(data)
 
         //convert checkbox value
         data.ShowOnHomepage = await this.homePage(data.ShowOnHomepage) 
         
         //Insert Data in the table
         const slides = await Slideshow.create(data)
         console.log(slides.$attributes.id)
         const slide = await Slideshow.findBy('id', slides.$attributes.id)
         //const slide = slides.$attributes 
         
         return view.render('adminpages.Slideshows.sliderDisplay', { slides: slide })
    }

    async show( {view, response, request, params}){
        const id = params.id
        const slide = await Slideshow.findBy(id)
        console.log(slide)
        return view.render('adminpages.Slideshows.sliderDisplay', { slides: slide })
    }

    async edit( {view, response, request, params}){
        const id = params.id
        const slide = await Slideshow.find(id)
        //console.log(id)
        return view.render('adminpages.Slideshows.editSlideshow', { slide: slide })
    }

    async update( {view, response, request, params} ){
        const id = params.id
        console.log('Testing')
        const changes = request.all()
        const show = changes.ShowOnHomepage == null ? 2 : 1
        console.log(show)
        const slide = await Database
                .table('sliders')
                .where('id', id)
                .update({
                    Category : changes.category,
                    Title : changes.title,
                    SubTitle : changes.subtitle,
                    Link : changes.link,
                    ImageUrl : changes.ImageUrl,
                    ShowOnHomepage : show,
                    updated_at : moment().format()
                })
        
        //console.log(slide)
        console.log(moment().format())

        const result = await Slideshow.findBy('id', id)
        return view.render('adminpages.Slideshows.sliderDisplay', { slides: result })
    }

    async destroy( {view, response, request, params}){
        //Get it of row to delete
        const id = params.id
        //Delete Sliders with id
        await Database.table('sliders').where('id', id).delete()
        return view.render('adminpages.Slideshows.slideshowIndex')
    }

    async homePage(data){
        return data === 'on' ?  data = 1:  data = 2;
    }
}

module.exports = AdminSlideShowController
