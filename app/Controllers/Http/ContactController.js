'use strict'

const Subscriber = use('App/Models/Subscriber')
const Database = use('Database')
const moment = use('moment')
const Mail = use('Mail')

class ContactController {
    async index({ view }){
        return view.render('FrontEnd.Contact');
    }

    async subscribe({ view, params, request ,response }){
        console.log(request.all())
        const email = request.body.email
        const ip = request.ip()
      
            const subscriber = new Subscriber()
            subscriber.email = email
            subscriber.IPAddress = ip
            subscriber.created_at = moment()
            subscriber.updated_at = moment()

            subscriber.save()

        return response.json({success: subscriber})
    }

    async sendemail({ view, request, response, params }){
        const param = request.all()

        await Mail.raw('Testing Email From Adonis', (message) =>{
            
            message.to('akendall@kendallgroupweb.us')
            message.from('antonio.kendall@gmail.com')
            message.subject('Testing Adonis Mail Provider')
        })

        console.log(param)
        console.log(Mail)

        return response.send('email successfully')
    }
}

module.exports = ContactController
