'use strict'

class LoginController {
    async create({ view, response, auth }){
        try {
            await auth.check()
          } catch (error) {
              //User is not logged in
              return view.render('auth.login')
          }
          //User is logged in
          return response.redirect('/')
    }

    async store({ auth, session, response, request }){
        await auth.attempt(request.input('email'), request.input('password'))
        session.flash({ successMessage: 'You have logged in successfully!' })
        return response.route('/')
    }

    async destroy({ response, auth, params }){
        await auth.logout()
        return response.route('login.store')
    }
}

module.exports = LoginController
