'use strict'

const Blog = use('App/Models/Blog')
const moment = use('moment')

class AdminBlogController {
    async index({ view }){
        const blog = await Blog.all()
        console.log(blog)
        return view.render('adminpages.AdminBlog.blogIndex', { blog: blog });
    }

    async create({ view }){
        return view.render('adminpages.AdminBlog.blogCreate')
    }

    async store({ view, request, response}){
        const data = request.except(['_csrf', 'files'])
        console.log(data)
        //convert checkbox value
        if(data.ShowOnHomePage === 'on'){
            data.ShowOnHomePage = 1;
        } else {
            data.ShowOnHomePage = 2;
        }

        const blog = await Blog.create(data)
        return view.render('adminpages.AdminBlog.blogShow', { blog: blog })
    }

    async show({ view, params, request }){
        const id = params.id
        const blog = await Blog.findBy('id', id)
        return view.render('adminpages.AdminBlog.blogShow', { blog: blog })
    }

    async edit({ view, params, request }){
        const id = params.id
        const blog = await Blog.findBy('id', id)
        let varDate = blog.PublishedDate
        varDate = moment(varDate).format('YYYY-MM-DD')
        blog.PublishedDate = varDate.trim()
        console.log(varDate.trim())
        return view.render('adminpages.AdminBlog.blogEdit', { blog: blog })
    }

    async update({ view, params, request }){
        const id = params.id
        const data = request.all()
        const blog = await Blog.findBy('id' , id )
        const varDate = moment(data.PublishedDate).format("YYYY-MM-DD")
        const show = data.ShowOnHomePage == null ? 2 : 1
        
        blog.Title = data.Title
        blog.SubTitle = data.SubTitle
        blog.BodyText = data.BodyText
        blog.AuthorUrl = data.AuthorUrl
        blog.AuthorName = data.AuthorName
        blog.Category = data.Category
        blog.PublishedDate = varDate
        blog.Link = data.Link
        blog.ImgUrl = data.ImgUrl
        blog.ShowOnHomePage = show

        await blog.save()
        return view.render('adminpages.AdminBlog.blogshow', { blog: blog })
    }

    async destroy({ view, params, request }){
        const id = params.id
        const blog = await Blog.findBy('id', id)
        console.log(blog)
        await blog.delete()
        return view.render('adminpages.AdminBlog.blogIndex')
    }
}

module.exports = AdminBlogController
