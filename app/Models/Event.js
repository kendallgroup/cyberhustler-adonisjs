'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Event extends Model {  
    static castDates(field, value){
        return value.format("DD/MM/YYYY")
    }
}

module.exports = Event
