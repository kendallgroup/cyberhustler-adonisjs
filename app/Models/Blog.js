'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = use('moment')

class Blog extends Model {
    static get dates() {
        return super.dates.concat(["PublishedDate","created_at", "updated_at"])
      }

    static formatDates(field, value){
        if(field == "PublishedDate"){
            return moment(value).format("YYYY/MM/DD")
        }
        return super.formatDates(field, value);
    }

    static castDates(field, value){
        if(field == "PublishedDate"){
            return moment(value).format("YYYY/MM/DD")
        }else if(field = "created_at"){
            return moment(value).format("YYYY/MM/DD")
        }else if(field = "updated_at"){
            return moment(value).format("YYYY/MM/DD")
        }
        return super.formatDates(field, value)
    }
}

module.exports = Blog
