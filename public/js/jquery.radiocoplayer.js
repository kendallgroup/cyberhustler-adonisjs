!(function(t) {
  "use strict";
  t.fn.radiocoPlayer = function(a) {
    function e(t, a) {
      var e = "";
      return (
        a && (e = t.match(/(\.|#)/g) ? (t.match(/\./g) ? "." : "#") : "."),
        e + "radioco-" + t
      );
    }

    function r(t) {
      return t.replace(/\./g, "").replace(/\#/g, ".");
    }

    function o() {
      (g = t.extend(
        {
          src: navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)
            ? t(R).data("src") + ".m3u"
            : t(R).data("src"),
          autoplay: null != t(R).data("autoplay") && t(R).data("autoplay"),
          playButton:
            null == t(R).data("playbutton") || t(R).data("playbutton"),
          volumeSlider:
            null == t(R).data("volumeslider") || t(R).data("volumeslider"),
          elapsedTime:
            null == t(R).data("elapsedtime") || t(R).data("elapsedtime"),
          nowPlaying:
            null == t(R).data("nowplaying") || t(R).data("nowplaying"),
          showPlayer:
            null == t(R).data("showplayer") || t(R).data("showplayer"),
          startVolume: null != t(R).data("volume") ? t(R).data("volume") : 50,
          image: null != t(R).data("image") ? t(R).data("image") : E,
          bg: null != t(R).data("bg") ? t(R).data("bg") : E,
          showArtwork:
            null != t(R).data("showartwork") && t(R).data("showartwork")
        },
        a
      )),
        t(
          '<audio id="radioco-' + r(R.selector) + '" preload="none"></audio>'
        ).appendTo("body"),
        t(
          '<source src="' +
            g.src +
            '" type="audio/mpeg">Your browser does not support the audio element.</source>'
        ).appendTo("#radioco-" + r(R.selector)),
        t(document).on("invalidStream", function(a) {
          t('<span class="radioco-error">' + a.message + "</span>").appendTo(
            "#" + h
          );
        }),
        i(),
        (I.id = d(g.src)),
        m(I.id),
        t(function() {
          g.autoplay &&
            setTimeout(function() {
              U.play();
            }, 1e3);
          var t = setInterval(function() {
              m(I.id);
              window[I.id] = m(I.id);
            }, 15e3),
            a = 0,
            e = setInterval(function() {
              (a += 1), a >= 14400 && (clearInterval(e), clearInterval(t));
            }, 1e3);
          S &&
            S.addEventListener(
              "error",
              function(t) {
                var a;
                switch (t.target.error.code) {
                  case t.target.error.MEDIA_ERR_NETWORK:
                    (a = "Network error"), n(a);
                    break;
                  case t.target.error.MEDIA_ERR_DECODE:
                    (a = "Media corruption error"), n(a);
                    break;
                  case t.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    break;
                  default:
                    (a = "Unknown error"), n(a);
                }
              },
              !0
            );
        });
    }

    function n(a) {
      t(".radioco-nowPlaying").html('<i style="opacity:0.4;">' + a + "</i>");
    }

    function i() {
      g.playButton &&
        (t(R.selector).prepend(
          '<div class="radioco-playButton"><div class="radioco-playButton-playing"></div></div>'
        ),
        t(document).on("audioPlay", function() {
          t(".radioco-playButton div").attr(
            "class",
            "radioco-playButton-paused"
          );
        }),
        t(document).on("audioPause", function() {
          t(".radioco-playButton div").attr(
            "class",
            "radioco-playButton-playing"
          );
        })),
        t(document).on("songChanged", function(t) {
          c("image", t.trackArtwork), c("bg", t.trackArtwork);
        }),
        t(R.selector).append('<div class="radioco-information"></div>'),
        g.nowPlaying &&
          t(".radioco-information").append(
            '<span class="radioco-nowPlaying"></span>'
          ),
        g.volumeSlider &&
          (t(".radioco-information").append(
            '<input type="range" class="radioco-volume" value="' +
              g.startVolume +
              '" min="0" max="100">'
          ),
          t(".radioco-information").on("change", ".radioco-volume", function() {
            U.volume(t(this).val());
          })),
        g.elapsedTime &&
          t(".radioco-information").append(
            '<span class="radioco-elapsedTime"></span>'
          ),
        g.showPlayer
          ? t(R.selector).append(
              '<img class="radioco-image" src="' +
                g.image +
                '"><img class="radioco-bg" src="' +
                g.bg +
                '">'
            )
          : !g.showPlayer &&
            g.showArtwork &&
            t(R.selector).append(
              '<img class="radioco-image" src="' + g.image + '">'
            ),
        g.showPlayer || l();
    }

    function l() {
      t(R.selector)
        .removeClass("radioplayer")
        .removeAttr("class"),
        t(".radioco-information")
          .removeClass("radioco-information")
          .addClass("nostyle"),
        t(".radioco-playButton").addClass("nostyle"),
        t(
          "<style>.nostyle{background:0 0;bottom:auto;box-shadow:none;clip:auto;display:inline;float:left;height:auto;left:auto;margin:inherit !important;margin-left:inherit !important;padding:0;right:auto;top:auto;width:auto}</style>"
        ).prependTo("head");
    }

    function c(a, e) {
      e
        ? t(".radioco-" + a).attr("src", e)
        : t(".radioco-" + a).attr("src", g[a]),
        (I.current_track.artwork_url = e);
    }

    function u(a, e) {
      e && t(a).unbind("click"),
        t(a).on("click", function() {
          g.playButton && U.playToggle();
        });
    }

    function s(a) {
      g.elapsedTime &&
        t(document).ready(function() {
          t(a).html(U.getTime(!0));
        });
    }

    function d(t) {
      return (t = t || ""), t.match(/(s[0-9a-f]{9})/g).join("");
    }

    function p(t, a) {
      return (
        (t += ""), t.length >= a ? t : new Array(a - t.length + 1).join("0") + t
      );
    }

    function m(a) {
      var e = "https://public.radio.co/stations/" + a + "/status";
      return t.ajax({
        url: e,
        success: function(a) {
          a.current_track || delete a.current_track,
            (I = t.extend(I, a)),
            g.nowPlaying &&
              t(".radioco-nowPlaying").html(I.current_track.title),
            t.event.trigger({
              type: "songChanged",
              message: "Track changed",
              previousTrack: v,
              trackArtwork: I.current_track.artwork_url_large
                ? I.current_track.artwork_url_large
                : a.logo_url,
              trackTitle: I.current_track.title,
              data: I,
              time: new Date()
            }),
            (v = I.current_track.title),
            U.hasLoaded() || U.load();
        },
        error: function(a) {
          t.event.trigger({
            type: "streamInfoError",
            message: "No stream information was recieved",
            errorData: a,
            time: new Date()
          });
        }
      });
    }

    function y(t) {
      var a = Math.floor((t % 86400) / 3600),
        e = Math.floor(((t % 86400) % 3600) / 60),
        r = ((t % 86400) % 3600) % 60;
      return p(a, 2) + ":" + p(e, 2) + ":" + p(r, 2);
    }
    var g,
      h = t(this).attr("id"),
      f = !1,
      R = t(this),
      k = !1,
      E =
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS8AAAEvCAIAAACbvNCPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAGIBJREFUeNrsnfl+GjcbhTEMy7DvGJukTtP76P1fRdMk3sCsZjOL851vVE9pgm1sNBqNdJ4//KNuDIOko/O+Wp0///wzRgjRgDiLgBCqkRBCNRJCNRJCqEZCqEZCCNVICNVICKEaCaEaCSFUIyFUIyEkAJwfP36wFAihNxJCqEZCqEZCCNVICNVICKEaCaEaCSFUIyFUIyGEaiSEaiSEUI2EUI2EEKqREKqREEI1EmIjDotAOr9u4D45OWGxEKoxKKWJFz8JD6pLpVJ7/3C1Wvn/eFec4jXlSmI8ieMQ+fnCSyQSyWQSesOLdDqN147juK77pjfcbDbr9frx8fHh4QH/uVgs8Bu8xs9dZVKf9Ebyr+kB6M31yGazb1XdsyXugRe5XG7399AnlAlZLj3gpfiNr0yKk2q00QPdJ6BAIRs1xOPxnIf/G1goZPnwBF77sqQ4qUZjRYjGDe2VSqVCoQBVaPJsSQ88kv+oM4/5fA4XPXmCjZhqNEGEEF4+ny8Wi2jx+jdrPGHeA6+32+3sCcS0VCbVGFUdIh+DE9brddGyo0gikSh64DWC2PF4PJ1O/VCWsqQaoxGRIhytVqvpdNqYr4bv0vRAnjmZTO7v7xHKUpZUo746dBynUqmUy2W4SmzfBL0Jdek4VY/NZjMajaBM2CaicWqSatRFh8lkstFoiKDOlkp1nLoHYlfIEnGsiAsoS6oxHJAcwgbRImGJ1tZuJpM59UD4Clkit6RVUo2qLRE/a7UapMiWJyh4ILHs9/u0SqpRXWiaz+dbrRYCVNbuT6BM4JPNZhOCHA6H/tQIS4ZqlC9FNKyzszN/upzsBcFqxWM2m93d3c3nc4avVKN8S2y322LIlByCWIi3WCwQviKxpCapRjlZIizRqlFTibiu2+l0Hh4e4JOTyYSapBrfyePjYyqVQmN6blchOZB0On1+ft5oNKBJZJXUJNX4ZikiRYQrst3IAp0ayrNer3e7XcauIagxoqtSIMVardZsNlmFQWgS4cZ8Pr+9vd3dxkWCJnqnVKH72G63rVaLUgyUbDb76dMnyDKZTKLvY4EwUt0jRYAmghiVZ4goQOzhGg6HiF1j3OVMNe5KET8/fvyIbps1p5JKpVIsFiHI0WjEZJKR6j+T+4idKMVQSCQS7Xb74uICWSUCVwYm9qpRSFE0BdZZiLiuiw4RSXvM0F1pVOOhASqlqE/g+scffyB132631KRFahTDNh8+fMhkMqwtrQLXs7MzdJFIIylIW9SIFAW1zlxRT3K53OfPn2mSVqgRUqzX61yAqnUDisd9k+S0pLFqRF+LTrfRaLCSomKSpVKJJmmgGsWlF+12mzUUIZNEfXU6nRiHW9+Lo60aUbUnJyes12iRz+d///33y8vL5XKpz6nt9Maj0sVqtcqRm6h28I7z22+/1Wo1LhJ4c9HpVl54nnQ6Xa/XWTeRBjWI/hQm6d+0RSLmjWLNzfn5OevPAKDGz58/4yfHWqOqRh73ZlTzisc/fPgAn+RYa8TUiNoS17axVsyLWqFJjslFTI2np6esEiPJ5/MXFxeOw5vto6BGpBaVSoXrwg0Glfvp06dcLsc0Ums1iotNuezGhjSy0+kwjXwORxM1ijszWEM2UKvV4JNXV1cxHu2hmzf6lyuyMuyhUChwN5amakRnyT7SNlzXhSATiQQFqYsaxepwzmrYCeJV/6AdloYWakSMSmO0FvTFcEgOtGqhRnF7GavB6iboDbQWi0UKMkw1whhRB9x3Q0C73S6Xy5YLMsy1EdvtlsZIfFqtFrrmfr9vbQcd2nwjegHXdZPJJIfUiI+Ydu71enbeihsPUY00RvIrtVoNJom4iWpUB7rAfD7Pxkd+Bd000kgLBRmOGmGMuVyO4zfkOUqlkoWCDE2NPCWVvCrI09NTqwQZjhrhigxTyauUy2WrcsgQ1Cj2+HP9DTkwh2w0GpbMQ4ajRiSNbGfkQGq1WrVatUGQTihqRJjKaUZyOGKD8ng8NnvkL65eiul02s65XXIMSCCNX8saQk/jui7bFnmfILPZrMFRlep1qujbqEbyPk5OTs7Ozr5+/bparYwcBQwhUuUFG+T97dXbfmXqiQGq1eh4sFWRY5qQqWclK1WjGMJheyJHkkql4JDmqVG1TUGNnNsgx5PJZFqt1u3trUlzHqq9EYXIlkSkUCqVKpWKSXMeqvsVRqpEIo1Gw3VdY6It1WrkbXBELmdnZ8YMscYVS5GLxYlcIMXz83ORB1GNb0gaaYwkoPTn9PSUamSYSrSgUCgYMKKj1Bs570+Cw4ARHXXrVMWVG5xsJMGBePXLly9wyIgOTyiNVOmNJOgG1m63o9vjK1UjtzWSoMnlctG9QYBqJAYmkKlUKooOqXQUhweovo/tdrvYwc5zuA9HbIOMZKRNb9QNRFlCdcvlcrPZPDw8iBa226+JF8lkEiaQTqfxM5PJcNWhDwqk1Wrd3NxEywDUqZHG+GrsMJlM7u/voUOhtxOPF7owX7fi36OEs9lsPp9H7sSOr1gsojDn83mExlcdxQ2OqtsbiA4Gg/F4jPI5eeLwqGzXOWez2XQ6FQcsFDxs7gRhj3/99ZcoVaqR3vh69zQcDiFFkVQf32h8JSPKhWfe3d3BKsvlsp1BLAKEZrN5e3tLNZJXQEJ4dXWFzFCKDp+TJaI1BMBQY71et/BEIhGvomOKhCDpV+EAP/z69Sti1KBDBrRCfMRqtbq8vPz27RvapW1FfXp6GpVDdKjGEKLTm5sbcZ+2sg5baBJu/P37d2hSjNPaE682Go1IqNHhyIpiKSI6nc1m0EYoJQ9ZIqX88uVLpVJB7GpJMh+V8VV1eePj4yOVD1cUUgzxGcSsyWg0QgNttVqW3FAEe/z7778Zqf5rC8wVIQBN7AiPgf4RRn19fW3D9U/JZFL/DZBK1WizIBEfilxRn0cSySS8GqaBxzO+CqrVquNonZopbRyW3Im5tyeCBemZtOCpttvtt2/fxuOx2bWAb9psNqnGf2vd2hh1s9loO4QgTLLb7SKtNbvHzHloK0h6Y+BAh8PhUP/RSzwh0lqYpNmdZqPRoDf+Hzu9EcYYlUeFINfr9devX/HT1OpIJpPabkdWujIOdWzbQA6+LwwnQtsI/DTy/Pw8lUoZWSmVSmUymWh4fI5Sb0TMZpsxjsfjyMXnaKN45u/fv5u6jA4hAASpoTFwFCdYZrNZFM8vEws7r66uTJ35QLCq4WyH0hMcbfNG9D7z+TysRXBSquzy8vLDhw9Ghqy1Wk23wwGUeuNqtbJKjZBipJ9fOCRCViO70UKhoNttoswbAwRhXtRvAfJzSCOzjHq9rpUalY6pol4hSHuObDEj6YIgUWsIWTudjmFXjLke+nSaccX1alWwul6vzWi+ouJ6vZ55dVStVvWxR9UprD37XJWNWuGDHncIaHU+BCmOtDOsmuCNmUxGE0EqjVRRowYv8lBsjEJ1juNks9lUKuV4oLNDggcfm8/nYnZb4jPgrbrdrji+1TB7vLq60iGKUX1KlVXeGGgGjh69Vquha9/9PX7pv55Op2LDe+y/hyMfo0axGeXjx48mJZDoztDFoAsL/UsxbwyK4AYhIYl6vd7pdH6S4k/k8/l2u31xcYEXeBgpvYMY0bm7uzOssjRZmqM6b0Snbk+wGlABQorlcvnAf59IJFqtFtwM3b+UNXoQ5Gg0MmyNDjqsZDIZuiAT5+fniuM38xKPvcBDpK8XR+kVCgXkOW+u5kSiWCzihZSTRfEOeJ9SqWRYlYV+jFUIS/XQraJJGa9GkWXJLV68G3LFY4Yr4ADi7O0jmx0yjsFg8I5+QVvQW/X7/XDtMYRFepYM5Ehf5CCM8ci3xTuISfwjm108Hh8OhyYtrkKZoHDsUiO+syVqlL5FAO+G9Ob498lkMrKu44aZmFRlyMat80Z8YRsE+fJlb+/j5UHUN71Po9E4clAHXxCJsUljcgjjs9lsiIKMh9JMLbkNQq49yr0poORx5OPheWiPMhtMKJ86n8/NG5Hb29dKtA44rdyGUq/XURHHHGaHP5xOp4h0jBkkR9SAPnS73YYyuBqON9pwli6Qe2tiECd6HH++KGozQsdwHYKYCrIlUhUNywZBoqOV6GZBLO7JZDJHni8KNc5mM5PO5oQawwpWw1EjqjDq++IPbOty3zCIdYXHbylCbZp0TjkygrDGckI7FMSSYFWuPQYx+oWU7/gtRYbttArLHkPzRjQsG85WlTUnEQtyLPrIWW+xUc6k7hXReyinV4V2iB0+F/mG8ZcH4gtKvJpqOp0GcTm5CMyObAkIVuWOWoULygSlrXhkNbRIVcwdG++NSQ9ZXV5AGRoyJTwkBwIkxgvRU6MlawAkLpEXagyilSB7PPJtYdomVShSDPXHqYV5tOvj46MNI6vHL3n5qdBGo1EQ9sju9dcsQ7E9hnwBPUJz49WINC+fz0sMVofDofTVoVJyUcPUKLHWoqFGG7wxFsCIebfblfuExz+eebtzEKwqHlkN+RICw5KNF+pV7ljOcrmUeziNlJ2K5u3OURysOuF+WzGyKn3NioZUKhUYmqwRc/TZ4/HYcRxZi+9lnbeN9zHpmJVsNqvy+s146GqczWY2BKvSz0GCIPv9vpRZImShUhbBmndeLtRoUaQa8wYJbRjLiQVwyDwE2ev1jt9hOBwOZXX/hqkRxXL83E+U1GjJMgCRhEivWhGy3tzcvPttV6uVxCtfzbvKSqU9JtrtduhfGA0C+Y9W91oGBILVyWQiNw8R8SHii3Q6jUzyrYHJ5eWl3P1Q5u0jl15l+npj7OnGFRvs0XXdIJZc+Ze6wSQPjxXFn8jd526eN6LKlI3iOJp8Z6jx8POzo549+lfWyBWkmL9F2Jn3eCHEQneAAke6iBfSz182b2tOJpNRc8ejFmoUXbsNWzpi3ho0CLLX6wVRu0KTKEkEroj8RaaK8BUfKs5QRTkvFguIVliiYbejBqpGi7xRrIe2QY0xbx051BJcd+vLTAxW/2RW4v8GlKUbKW8EqxKHnXXPG2NP60vsucGq2WxCD0EHdb7wdgm0VRmpRmX7NjUaxjTseJVX41UI0rAvhc7FyIFxZbOOeqkRkZVJp4+9Gv+Uy2XDvu9bp1hojzrmjX7nOplMbDj4WAA1Lj2MCfCk3z6iCcIbg64mveIKq4JVP4GUfoh4iJ2pqTdzqvFG7dS43W4tWSj3TwXE4+12W8GIjhpM3Y4DNaqYb9SwEQyHQxuuW92N7k5PT6+urqQvCQglojO4mtbrdaAVpN0ImFgJYJU9ikYMQcZk7MEPMUxVvP9IMUceqxdJNQpBBnEQk/4xXqvViu7KMqrRWDUiJLBkF/IuaM1iS00UBYlaM3spldzN4vuDYW2rdjAYmN3XPueQEKTYrxihHBJPCymKpbD0RqO80WZ7jHnDd2dnZ9EaZcWjGr8FR4E36ruOybxrOt9U8RBkVOYhRcaowDrCBf1j0CuNtFbjZrOxZBfynhTCcTqdDlp5JJbO1Wo1Gyol6P5R6zW+YnDVnpWrv379ZrOJho4S0NYk8WyIUU1dnvprF2mpN4rmiMq2ba3cTxSLRW3TSDwSslxLDm2Iybit5BW16+8PUGOhUFB/YZA+pFKp8/Pzfr8vDnfTZKxVjPrCvW24FVeRGvUvSjzhYDBoNBo2OyS8ESWQz+fv7u7kniv17krBz9PTU6t6SdQCgrXg9nBGYG+o2Pdo2AUP78N13U6nI+7YCbEbFR/darVMusxYB2+Mxk5tCFLuJTDRBUVRrVaRSUKZoYzuiA3+7XYbD2Bb4VON/zTB1Wpl21LylzNJWBM0mclkVGoSn4WPxufa5op+Oww2b4xQQQyHw2w2a/Nwzk9AEsjcEMOPRqP5fB7oAI+IjcvlcqVSsbbAbR9T3VUjOmYO5+zVJHxSbEMD0g9KFTpEP1ir1SyZV6Q3HlQW0+m0UCjYGSa92m2XPWCS4rBWf+/y+9qQP1CUy+WKxaIoc3smM0IhYl0dGla/30fewpp7jqxHzLvYdOGBUPYnWe7Vp680oUPXdfE+kCJTA3rjs8WxWq2QJtmz/uPdZDxEmgdBrjzW67VYaYgXu/JLJpPxeFxcE4C/UnMMDNUYbTXGnm4sRLdt8BEsQeSWDO8j0LYj+tycfiRUo17xKuuPqCTo7URRPRlabLayYZMr0YcfT9Ab99Dr9dhEiEo1MlJ9KV4dDodsJcSMSDXa3ijGV9XcO0sI1fi6QyJe3W63bCuEagxfjSgjTngQqlEXQS4WC2tPlyPKCPrUBUOW5COBHA6H6XSaC3RIcKzXa3rjoSCBtPa4R6LGG6nGQ+NVFBYTSEI1apRAcsUcCYjNZhNs3mjY/lGxYg7Zo4X3W5FAgVKgxuCObzTNG31BIoEMOuEmtqGgRZmpRvzsdrsc0SFUoxaCRFDBNeVEIqvVinv/3y/I5XLZ7/er1SpbEjkeBafdxw0uPgjy/v6ea3SIrEg1aG+Mm12CYo3OfD5nYyLHsPWgN0pwyLu7O16qQzQPU21RY8wbYuWcB3k3y+VSwZGWcRuKEuX448cPrmIl9EZdBLnZbG5vb3l2PXkraDMKpjdiBs9w7BUkglUIstls8iBtcjjKRgEd27wCIQdySAqSHM5isYgpuREoblvJQoQQJJfpkDepUU3fHbewcMUyHQqSHBhMKTsDLW5nEYudkNyaTF5lNpspS2ri1pYyihjZeb/fZ4MjLyCucKcaVQgSPR8dkjwHMhqVR/XGLS9u4ZDMIcleptOpyrF3hyUucshut1uv1zntQXweHx8ROgV69Aa9cb8gEZNAkFypQ8IyRqrxP4JcrVZcOkd8YIyKP5Fq/I8gxdI5Li4nSF4UbC+mGl8X5PX1NbdfWc5kMlE/iEA17hEkvBEOyQ3K1oKcBbWvXo0O06S9CEHWarVcLsfSsA1xYbZ6adAbn3VIcYQHD7myDbiimp3+e7yRpf9SX+UdcoUcksdA2sNgMAhr2pne+LogZ7MZB1otYbFYqNnmTzW+P2pFDV1fX+MnS8NgkCgiFApxPRbVeKggxbgOj2Y1mPF4HPRd4lSjNEGKcR0x4EYMY7PZ3N/fh7tQmWp8cxo5nU6ZRprHYDAIv3WxGt6dRi6XS5aGGaCHDWtWg2qUk0b2ej3eam5GjIrsQ+XOKaoxkDQSmQaiVpXbw4l09Dn8gbP/EqLWq6urWq3mui4LJHIgulG/V4PeGKAgRf+KaIeLfqPFYrEIfRz1P97IBiRLk9PpFLULk0yn0ywQ/YElihMD9ZEAvVGmIJFAIo2kSeqPGIRDNWl1EhLVKFmQYkLy+vqa2yN1BlIMd9kN1UiTJDGR5Ie4NJxqDNMkxQ1HRBMGg4GyW26oRu1Msuex2WxYIKGDaEX9uYxUo14muVwuYZLj8ZiBa7iueH9/r8Oam+fg7L8iTca8Y8hms1mlUuE6AcWgE0SuiD5RZylSjeEErplMplwuJ5NJlokCxGSGPgtuGKnqFbg+PDzc3NwgcOIC16BBCXe73UhIkd4YmibF3Vggn88Xi0XNI6iIAhHCFeGNUbnsiGoMOZmcepRKpUKhwDKRCHo6Md8boXvHqEYtNDkej+/v7+GT0CRvrTs+UYQOxZ3E0SpMqlEXTaINTTwQuEKWjF3fB3JykZBHsQCpRr1MMuZNhICCBzX5JkSIsVuYEVMj56P1BIJEw8pms/DJVCrFAnmZzWbT7/f91acRbdX0Rq2tcjabTadTqBGazOVyTCl/BcKLuiVSjZHRpLhScugBQdIqd0FvNRqNIjSHQTWak1L6VpnzsDmrXCwWsEQRmhoTMlCNkbTKkQeyykKhYJtVQodIqsVtp4b1R1RjhK1yPp/DLZPJpA1WiXBUhAabzcY8HVKNhlglWidiNlhlOp3OZDKQZSKRMOlr4gvCDP37iAzudKhGQ6xSnOwKoEy4peu6iGMjvU0EZrhcLuGH4kx+G8aTqUYDZSmuW4KfwEaEYQLHcaIiQrGeHj2LWGVqz2AV1WiyLPFz6YFmjfBVKDOVSmnomehBoEA8qhie8eNwqyqOarRFmRDkwkOsU0nuAH2qTzXhgXA/aE8E2P6coc3TNlSjpZ653W5hR7448ctdcQLpvgThQXL4UCE/cWyX/0hckUs1Upz/zpesPWJPizzhlnEPvBBaFb9E/vnCKlDIzH+NdxPyA/6fUH5UIzlUnP4L6OfXUydfXY2911EtzACpRhKUPl8WG5EFowVCtPFG7m8khN5ICKEaCaEaCSFUIyFUIyGEaiSEaiSEUI2EUI2EEKqREKqREEI1EkI1EkKoRkKoRkII1UgI1UgIoRoJIVQjIRrBc3EIoTcSQqhGQqhGQgjVSAjVSAihGgmhGgkhVCMhVCMhhGokhGokhATB/wQYANqhtyrt4uccAAAAAElFTkSuQmCC",
      I = {
        id: null,
        current_track: {
          title: "Live Broadcast",
          artwork_url: null
        },
        logo_url: E
      },
      v = null;
    t(R.selector).length && o();
    var S = document.getElementById("radioco-" + r(R.selector)),
      U = {
        load: function(a) {
          if (
            (u(e("playButton", !0)),
            s(e("elapsedTime", !0)),
            t.event.trigger({
              type: "audioLoaded",
              message: "Radio loaded and ready to play",
              time: new Date()
            }),
            S.addEventListener(
              "timeupdate",
              function(a) {
                t.event.trigger({
                  type: "timeUpdate",
                  message: "Time",
                  newTime: U.getTime(),
                  time: new Date()
                }),
                  s(e("elapsedTime", !0));
              },
              !0
            ),
            (k = !0),
            t.event.trigger({
              type: "songChanged",
              message: "Track changed",
              previousTrack: v,
              trackArtwork: I.current_track.artwork_url,
              trackTitle: I.current_track.title,
              time: new Date()
            }),
            "function" == typeof a)
          )
            return a.call(this);
        },
        hasLoaded: function() {
          return k || !1;
        },
        play: function(a) {
          this.volume(g.startVolume);
          this.getStationId(t(R).data("src"));
          if (
            ((S.src = navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)
              ? t(R).data("src") + ".m3u"
              : t(R).data("src")),
            S.load(),
            S.play(),
            (f = !0),
            t.event.trigger({
              type: "audioPlay",
              message: "Radio play event triggered",
              previousTrack: v,
              trackArtwork: I.current_track.artwork_url,
              trackTitle: I.current_track.title
                ? I.current_track.title
                : "undefined",
              data: I,
              time: new Date()
            }),
            "function" == typeof a)
          )
            return a.call(this);
        },
        pause: function(a) {
          if (
            (f && (S.pause(), (S.src = ""), (f = !1)),
            t.event.trigger({
              type: "audioPause",
              message: "Radio pause event triggered",
              timePaused: this.getTime(),
              currentSong: null,
              currentArtist: null,
              previousTrack: v,
              trackArtwork: I.current_track.artwork_url,
              trackTitle: I.current_track.title,
              data: I,
              time: new Date()
            }),
            "function" == typeof a)
          )
            return a.call(this);
        },
        playToggle: function(t, a) {
          if (f) {
            if ((this.pause(), "function" == typeof a)) return a.call(this);
          } else if ((this.play(), "function" == typeof t)) return t.call(this);
        },
        event: function(a, e) {
          t(document).on(a, e);
        },
        getTime: function(t) {
          return Math.round(S.currentTime) >= 21600
            ? (n("Player timed out"), void this.pause())
            : 1 == t
            ? y(Math.round(S.currentTime))
            : Math.round(S.currentTime);
        },
        volume: function(t) {
          return t <= 0
            ? ((S.muted = !0), 0)
            : ((S.muted = !1),
              (S.volume = t ? t / 100 : S.volume),
              100 * S.volume);
        },
        mute: function() {
          return (S.volume = 0), 0;
        },
        isPlaying: function() {
          return f || !1;
        },
        getStationId: function() {
          return I.id || "Invalid station ID";
        },
        nowPlaying: function() {
          this.event("songChanged", function(t) {
            return t.trackTitle;
          });
        },
        getStreamState: function() {
          return S.readyState;
        },
        getArtwork: function(t, a, e) {
          var r, o, n;
          if (t && a)
            return (
              (r = I.current_track.artwork_url || "invalid"),
              r
                ? ((o = e || 75),
                  (n = t + "x" + a + "-" + o),
                  (r = r.replace("100x100-75", n)))
                : (r = I.logo_url),
              r
            );
        }
      };
    return U;
  };
})(jQuery);
