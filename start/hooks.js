const { hooks} = require('@adonisjs/ignitor')

const moment = use('moment')

hooks.after.providersBooted(() => {
    const View = use('View')

    View.global('formatDate', (value) => {
        var date = new Date(value)
        var arr = ['00','01', '02', '03', '04', '05', '06', '07', '08', '09'];
        var day = date.getDate() < 9 ? arr[date.getDate()] : date.getDate();
        var month = date.getMonth()+1 > 10 ? date.getMonth()+1 : arr[date.getMonth()+1];
       return date.getFullYear() + "-" + month +  "-" + day
    })

    View.global('preview', (value) => {
        return value.substring(0, 150)
    })

    View.global('Day', (value) =>{
        console.log(value)
        var date = new Date(value)
        var arr = ['00','01', '02', '03', '04', '05', '06', '07', '08', '09']
        var day = date.getDate() < 9 ? arr[date.getDate()] : date.getDate()
        return day
    })
})