'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'SectionController.home')
Route.get('/register', 'RegisterController.create').as('register.create')
Route.post('/register', 'RegisterController.store').as('register.store').validator('User')

Route.get('/login', 'LoginController.create').as('login.create')
Route.post('/login', 'LoginController.store').as('login.store')
Route.post('/logout', 'LoginController.destroy').as('logout')
/****************************Music***************************************/
Route.group(() => {
    Route.get('/', 'MusicSubmissionController.index').as('submission.index')
    Route.post('/submit', 'MusicSubmissionController.store').as('submission.store')
    Route.get('/:id', 'MusicSubmissionController.show').as('submission.show')
}).prefix('submission').middleware(['auth'])
/***************************************************************************/

/****************************Blog/Post***************************************/
Route.group(() => {
    Route.get('/', 'BlogController.index')
    Route.get('/:id', 'BlogController.blog')
}).prefix('blog').middleware(['auth'])
/****************************************************************************/

/****************************Event/Post***************************************/
Route.group(() => {
    Route.get('/', 'EventController.index')
    Route.get('/:id', 'EventController.event')
}).prefix('event').middleware(['auth'])
/****************************************************************************/

/****************************Charts/Post***************************************/
Route.group(() => {
    Route.get('/', 'HustlersChartController.index')
    Route.get('/:id', 'HustlersChartController.HustlersChart')
}).prefix('HustlersChart').middleware(['auth'])
/****************************************************************************/

/****************************Video/Post***************************************/
Route.group(() => {
    Route.get('/', 'VideoController.index')
    Route.get('/:id', 'VideoController.video')
}).prefix('video').middleware(['auth'])
/****************************************************************************/

/****************************Contact/Email***************************************/
Route.group(() => {
    Route.get('/', 'ContactController.index')
    Route.post('/sendemail', 'ContactController.sendemail').as('contact.sendemail')
    Route.post('/subscribe', 'ContactController.subscribe')
}).prefix('contact').middleware(['auth'])
/****************************************************************************/


/****************************************************************************************************/
Route.get('/admin', 'AdminController.index').as('adminhomepage')

Route.group(() => {
    Route.get('/', 'AdminSubmissionController.index').as('submission.index')
    Route.get('/:id', 'AdminSubmissionController.show').as('submission.show')
    Route.get('/:id/edit', 'AdminSubmissionController.edit').as('submission.edit')
    Route.post('/:id', 'AdminSubmissionController.update').as('submission.update')
    Route.delete('/:id', 'AdminSubmissionController.destroy').as('submission.destroy')
}).prefix('admin/submission')

Route.group(() => {
     Route.get('/', 'AdminSlideShowController.index').as('sliders.index')
     Route.get('/create', 'AdminSlideShowController.create').as('sliders.create')
     Route.post('/store', 'AdminSlideShowController.store').as('sliders.store')
     Route.get('/:id', 'AdminSlideShowController.show').as('sliders.show')
     Route.get('/:id/edit', 'AdminSlideShowController.edit').as('sliders.edit')
     Route.post('/:id', 'AdminSlideShowController.update').as('sliders.update')
     Route.delete('/:id', 'AdminSlideShowController.destroy').as('sliders.destroy')
}).prefix('admin/sliders')

Route.group(() => {
    Route.get('/', 'AdminUpcomingShowController.index').as('upcomingShow.index')
    Route.get('/create', 'AdminUpcomingShowController.create').as('upcomingShow.create')
    Route.post('/store', 'AdminUpcomingShowController.store').as('upcomingShow.store')
    Route.get('/:id', 'AdminUpcomingShowController.show').as('upcomingShow.show')
    Route.get('/:id/edit', 'AdminUpcomingShowController.edit').as('upcomingShow.edit')
    Route.post('/:id', 'AdminUpcomingShowController.update').as('upcomingShow.update')
    Route.delete('/:id', 'AdminUpcomingShowController.destroy').as('upcomingShow.destroy')
}).prefix('admin/UpcomingShow')

Route.group(() => {
    Route.get('/', 'AdminVideoController.index').as('video.index')
    Route.get('/create', 'AdminVideoController.create').as('video.create')
    Route.post('/store', 'AdminVideoController.store').as('video.store')
    Route.get('/:id', 'AdminVideoController.show').as('video.show')
    Route.get('/:id/edit', 'AdminVideoController.edit').as('video.edit')
    Route.post('/:id', 'AdminVideoController.update').as('video.update')
    Route.delete('/:id', 'AdminVideoController.destroy').as('video.destroy')
}).prefix('admin/Video')

Route.group(() => {
    Route.get('/', 'AdminChartController.index').as('charts.index')
    Route.get('/create', 'AdminChartController.create').as('charts.create')
    Route.post('/store', 'AdminChartController.store').as('charts.store')
    Route.get('/:id', 'AdminChartController.show').as('charts.show')
    Route.get('/:id/edit', 'AdminChartController.edit').as('charts.edit')
    Route.post('/:id', 'AdminChartController.update').as('charts.update')
    Route.delete('/:id', 'AdminChartController.destroy').as('charts.destroy')
}).prefix('admin/charts')

Route.group(() => {
    Route.get('/', 'AdminBlogController.index').as('blog.index')
    Route.get('/create', 'AdminBlogController.create').as('blog.create')
    Route.post('/store', 'AdminBlogController.store').as('blog.store')
    Route.get('/:id', 'AdminBlogController.show').as('blog.show')
    Route.get('/:id/edit', 'AdminBlogController.edit').as('blog.edit')
    Route.post('/:id', 'AdminBlogController.update').as('blog.update')
    Route.delete('/:id', 'AdminBlogController.destroy').as('blog.destroy')
}).prefix('admin/blog')

Route.group(() => {
    Route.get('/', 'AdminEventController.index').as('event.index')
    Route.get('/create', 'AdminEventController.create').as('event.create')
    Route.post('/store', 'AdminEventController.store').as('event.store')
    Route.get('/:id', 'AdminEventController.show').as('event.show')
    Route.get('/:id/edit', 'AdminEventController.edit').as('event.edit')
    Route.post('/:id', 'AdminEventController.update').as('event.update')
    Route.delete('/:id', 'AdminEventController.destroy').as('event.destroy')
}).prefix('admin/event')

//leave at the bottom of Routes
Route.get('*', 'SectionController.errorpage')
/******************************************************************************************************/


