'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChartSchema extends Schema {
  up () {
    this.create('charts', (table) => {
      table.increments().primary()
      table.string('ArtistName').notNullable()
      table.string('TrackName').notNullable()
      table.string('Icon').notNullable()
      table.integer('Rank').notNullable()
      table.string('ImgUrl').notNullable()
      table.string('VideoUrl').nullable()
      table.boolean('ShowOnHomePage').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('charts')
  }
}

module.exports = ChartSchema
