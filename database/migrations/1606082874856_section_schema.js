'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SectionSchema extends Schema {
  up () {
    this.create('sections', (table) => {
      table.increments('ID').primary()
      table.string('sectionID').notNullable()
      table.string('sectionName').notNullable()
      table.string('sectionHash').notNullable()
      table.string('sectionValue').notNullable()
      table.string('section').notNullable()
      table.string('sectionNumber').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('sections')
  }
}

module.exports = SectionSchema
