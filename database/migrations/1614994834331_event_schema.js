'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventSchema extends Schema {
  up () {
    this.table('events', (table) => {
      // alter table
      table.string('TicketLink')
    })
  }

  down () {
    this.table('events', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EventSchema
