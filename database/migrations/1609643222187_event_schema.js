'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventSchema extends Schema {
  up () {
    this.create('events', (table) => {
      table.increments().primary()
      table.string('EventName').notNullable()
      table.string('Host').notNullable()
      table.string('Description').notNullable()
      table.string('Address').nullable()
      table.string('City').nullable()
      table.string('State').nullable()
      table.date('EventDate').notNullable()
      table.time('EventTime').notNullable()
      table.string('Link').notNullable()
      table.string('ImgUrl').notNullable()
      table.string('ShowOnHomePage').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('events')
  }
}

module.exports = EventSchema
