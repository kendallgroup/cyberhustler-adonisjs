'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VotesSchema extends Schema {
  up () {
    this.create('votes', (table) => {
      table.increments()
      table.string('Type', 30).notNullable()
      table.string('Userid', 30).notNullable()
      table.string('IPAddress', 30).notNullable()
      table.string('Hashid', 255).notNullable()
      table.integer('Vote', 5).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('votes')
  }
}

module.exports = VotesSchema
