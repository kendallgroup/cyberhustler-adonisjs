'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SliderSchema extends Schema {
  up () {
    this.create('sliders', (table) => {
      table.increments('ID').primary().notNullable()
      table.string('Category').notNullable()
      table.string('Title').notNullable()
      table.string('Subtitle').notNullable()
      table.string('Link').notNullable()
      table.string('ImageUrl').notNullable()
      table.boolean('ShowOnHomepage').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('sliders')
  }
}

module.exports = SliderSchema
