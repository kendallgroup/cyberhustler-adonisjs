'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SubscribersSchema extends Schema {
  up () {
    this.create('subscribers', (table) => {
      table.increments()
      table.string('email').notNullable()
      table.string('IPAddress')
      table.timestamps()
    })
  }

  down () {
    this.drop('subscribers')
  }
}

module.exports = SubscribersSchema
