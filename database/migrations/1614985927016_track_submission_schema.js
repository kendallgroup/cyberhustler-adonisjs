'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TrackSubmissionSchema extends Schema {
  up () {
    this.create('track_submissions', (table) => {
      table.increments()
      table.string('FirstName', 50).notNullable()
      table.string('LastName', 50).notNullable()
      table.string('ArtistName', 50).notNullable()
      table.string('Email').notNullable()
      table.string('TrackName').notNullable()
      table.string('VideoUrl').notNullable()
      table.string('AudioUrl')
      table.string('ArtistLink')
      table.timestamps()
    })
  }

  down () {
    this.drop('track_submissions')
  }
}

module.exports = TrackSubmissionSchema
