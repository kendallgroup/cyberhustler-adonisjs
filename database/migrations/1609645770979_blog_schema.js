'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BlogSchema extends Schema {
  up () {
    this.create('blogs', (table) => {
      table.increments().primary()
      table.string('Title').notNullable()
      table.string('SubTitle').notNullable()
      table.text('BodyText').notNullable()
      table.string('AuthorUrl').notNullable()
      table.string('AuthorName').notNullable()
      table.string('Category').notNullable()
      table.datetime('PublishedDate').nullable()
      table.string('Link').notNullable()
      table.string('ImgUrl').notNullable()
      table.boolean('ShowOnHomePage').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('blogs')
  }
}

module.exports = BlogSchema
