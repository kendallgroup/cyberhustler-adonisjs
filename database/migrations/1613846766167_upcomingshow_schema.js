'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UpcomingshowSchema extends Schema {
  up () {
    this.create('upcomingshows', (table) => {
      table.increments().primary()
      table.string('ImgUrl').notNullable()
      table.string('Title').notNullable()
      table.string('Link').notNullable()
      table.string('ShowHost').notNullable()
      table.string('Category').notNullable()
      table.time('ShowTime').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('upcomingshows')
  }
}

module.exports = UpcomingshowSchema
