'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChartheaderSchema extends Schema {
  up () {
    this.create('chartheaders', (table) => {
      table.increments()
      table.string('ChartName').notNullable()
      table.string('ImgUrl').notNullable()
      table.string('Link').notNullable()
      table.string('Category').notNullable()
      table.string('HashId').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('chartheaders')
  }
}

module.exports = ChartheaderSchema
