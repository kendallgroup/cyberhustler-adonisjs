'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VideoSchema extends Schema {
  up () {
    this.create('video', (table) => {
      table.increments()
      table.string('Title').notNullable()
      table.string('Link').notNullable()
      table.string('ImgUrl').notNullable()
      table.string('Category').notNullable()
      table.string('VideoUrl').notNullable()
      table.boolean('ShowOnHomePage').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('video')
  }
}

module.exports = VideoSchema
