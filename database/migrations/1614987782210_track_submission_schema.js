'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TrackSubmissionSchema extends Schema {
  up () {
    this.table('track_submissions', (table) => {
      // alter table
      table.string('agreeToTerms').notNullable()
    })
  }

  down () {
    this.table('track_submissions', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TrackSubmissionSchema
